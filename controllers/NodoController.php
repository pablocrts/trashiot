<?php

namespace app\controllers;

use Yii;
use app\models\Nodo;
use app\models\Dispositivo;
use app\models\NodoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\User;

/**
 * NodoController implements the CRUD actions for Nodo model.
 */
class NodoController extends Controller

{
    public $enableCsrfValidation = false;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view','create','delete'],
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view','create','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $valid_roles = [User::ROLE_ADMIN, User::ROLE_SUPERUSER];
                            return User::roleInArray($valid_roles) && User::isActive();
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Nodo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NodoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $rows = Yii::$app->db->createCommand('SELECT t1.id, t1.medicion,date_format(t1.fecha, "%d/%m/%Y %H:%i") AS fecha, t1.dispositivo_id FROM nodo as t1 
        INNER JOIN ( SELECT MAX(fecha)as fecha, dispositivo_id FROM nodo GROUP BY dispositivo_id)as t2 ON t1.fecha = t2.fecha AND t1.dispositivo_id = t2.dispositivo_id')->queryAll();
    
       return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
                'rows' => $rows
        ]);
    }

    /**
     * Displays a single Nodo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Nodo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Nodo();
       
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $hola = "Se agregó correctamente";
            return $hola;
        }else{
            $request = Yii::$app->request;
            
            $params = $request->bodyParams;
            $pena = "medicion";
            return $params;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreate3()
    {
        $request = Yii::$app->request;
        $model = new Nodo();
       
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $hola = "Se agregó correctamente";
            return $hola;
        }else{
            $request = Yii::$app->request;
            
            $params = $request->bodyParams;
            $pena = "medicion";
            return $params;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreate2()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $student = new Nodo();
        
        $student->attributes = \yii::$app->request->post();
        $disp =  $student->dispositivo_id;
        $med  =  $student->medicion;

        $valor = Dispositivo::findOne($disp);
        $imp  = $valor->distancia;

        $final = intval(100 - (($med * 100)/$imp));
        $student->medicion =  $final;
        date_default_timezone_set("America/Santiago");
        $student->fecha = date("Y-m-d H:i:s");

        if($student->validate())
        {
         $student->save();
         return array('status' => true, 'data'=> 'Student record is successfully updated');
        }
        else
        {
         return array('status'=>false,'data'=>$student->getErrors());    
        }
        
    }

    /**
     * Updates an existing Nodo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Nodo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Nodo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Nodo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Nodo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
