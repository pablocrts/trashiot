<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Dispositivo;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Nodo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nodo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'medicion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dispositivo_id')->dropDownList(ArrayHelper::map(Dispositivo::find()->all(),'id','id'),['prompt'=>'Elige uno por favor']) ?>

    <div class="form-group">
        <?= Html::submitButton('Actualizar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
