<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nodo".
 *
 * @property int $id
 * @property int|null $medicion
 * @property string|null $fecha
 * @property int $dispositivo_id
 *
 * @property Dispositivo $dispositivo
 */
class Nodo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nodo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['medicion', 'dispositivo_id'], 'integer'],
            [['fecha'], 'safe'],
            [['dispositivo_id'], 'required'],
            [['dispositivo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dispositivo::className(), 'targetAttribute' => ['dispositivo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Identificador',
            'medicion' => 'Medición',
            'fecha' => 'Fecha',
            'dispositivo_id' => 'Dispositivo ID',
        ];
    }

    /**
     * Gets query for [[Dispositivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDispositivo()
    {
        return $this->hasOne(Dispositivo::className(), ['id' => 'dispositivo_id']);
    }
}
