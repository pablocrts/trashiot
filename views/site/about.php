<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Acerca de';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1>A cerca de </h1>

    <p>
       Proyecto final del Taller de Internet de las Cosas <br>
       <b>Integrantes:</b> Pablo Cortés, Diego Poblete
    </p>

</div>
