<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\chartjs\ChartJs;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NodoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Medición';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nodo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <!-- 
    <p>
        <?php // Html::a('Create Nodo', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="jumbotron">
        <h3>Porcentaje de llenado </h3> 
   
    </div>

    <?php 
       for($i=0;$i<count($rows);$i++){
      //  echo $rows[$i]['medicion'];
          $llenado =$rows[$i]["medicion"]; 
          $vacio = 100 - $llenado;
          $dispositivo = $rows[$i]["dispositivo_id"];
          $fecha = $rows[$i]["fecha"];
    ?>
        <div class="row">
        <h4>Dispositivo <?= $dispositivo; ?> </h4>
        <h5>Porcentaje de llenado :  <?=  $llenado ?>%</h5>
        <h5>Fecha medición: <?= $fecha; ?>  </h5>
        <?= ChartJs::widget([
 'type' => 'pie',
 'options' => [
     'height' => 200,
     'width' => 600,
     'responsive' => true,
     'animation'=> true,
     'barValueSpacing' => 5,
     'barDatasetSpacing' => 1,
     //'tooltipFillColor'=> "rgba(0,0,0,0.8)",
     //'multiTooltipTemplate' => "<%= Value %> - <%= value %>",
     // String - Template string for single tooltips,
     //'tooltipTemplate'=>  "<%if (label){%><%=label%>: <%}%><%= value %>",

     // String - Template string for single tooltips,
     //'multiTooltipTemplate'=>  "<%= value %>",

     'tooltips'=> [
      'callbacks'=> [
          'title' =>  '***** My custom label title *****'           

          ]
    ],

],
 'data' => [
       'datasets' => [
          [
              'label' => 'Valor Inventario',
              'data'=> [$vacio, $llenado ],
              'backgroundColor'=> [
                'rgba(255, 204, 0)',
                'rgba(18, 34, 108)',
                'rgba(255, 206, 86, 0.2)'
            ],
            'borderColor'=> [
                'rgba(5, 5, 5)',
                'rgba(5, 5, 5)',
                'rgba(255, 206, 86, 1)'
            ]
          ]
      ],

       // These labels appear in the legend and in the tooltips when hovering different arcs
       'labels' => [
           'Vacio',
           'Lleno'
         
       ]
 ]
]);?>
 <?php  
 }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'medicion',
            'fecha',
            'dispositivo_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
